package application;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		File file = new File("/tmp/in");
		Scanner sc = new Scanner(System.in);
		
		try {
			sc = new Scanner(file);
			while(sc.hasNextLine()) {
				System.out.println(sc.nextLine());
			}
		}catch (FileNotFoundException e) {
			System.out.println("Error ao abrir arquivo: " + e.getMessage());
		}finally {
			
			if(sc != null) {
				sc.close();
			}
		}
		System.out.println("Bloco (finally) executado com sucesso");
	}

}
