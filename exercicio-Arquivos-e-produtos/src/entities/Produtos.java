package entities;

public class Produtos {
	private String name;
	private double preco;
	private int quantidade;
	
	public Produtos(String name, double preco, int quantidade) {
		
		this.name = name;
		this.preco = preco;
		this.quantidade = quantidade;
	}
	
	public Produtos(String name, double preco) {
		
		this.name = name;
		this.preco = preco;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	public int getQuantidade() {
		return quantidade;
	}
	
	public double total() {
		
		return preco * quantidade;
		
	}

}
