package application;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import entities.Circle;
import entities.Rectangle;
import entities.Shape;

public class Program {

	public static void main(String[] args) {
		
		List<Shape> myShape = new ArrayList<>();
		myShape.add(new Rectangle(3.0, 2.0));
		myShape.add(new Circle(2.0));
		
		List<Circle> myCircle = new ArrayList<>();
		myCircle.add(new Circle(2.0));
		myCircle.add(new Circle(3.0));
		
		System.out.println("Total área: " + totalArea(myCircle));

	}

	private static double totalArea(List<? extends Shape> list) {
		
		double soma = 0.0;
		for(Shape s : list) {
			soma += s.area();
		}
		
		return soma;
	}

}
