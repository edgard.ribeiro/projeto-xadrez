package application;

import java.util.Date;

import entidade.Ordem;
import entidade.enums.StatusOrdem;

public class Program {

	public static void main(String[] args) {
		
		Ordem ordem = new Ordem(1080, new Date(), StatusOrdem.AGUARDANDO_PAGAMENTO);
		
		System.out.println(ordem);
		
		StatusOrdem os1 = StatusOrdem.ENTREGUE;
		StatusOrdem os2 = StatusOrdem.valueOf("ENTREGUE");
		
		System.out.println(os1);
		System.out.println(os2);
	}

}
