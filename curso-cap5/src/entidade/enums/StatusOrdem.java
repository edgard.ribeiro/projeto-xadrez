package entidade.enums;

public enum StatusOrdem {
	AGUARDANDO_PAGAMENTO,
	PROCESSANDO,
	ENVIADO,
	ENTREGUE;

}
