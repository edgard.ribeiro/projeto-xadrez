package application;

import java.util.Locale;
import java.util.Scanner;

import entities.Produtos;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Digite o produto: ");
		
		System.out.println("Nome: ");
		String name = sc.nextLine();
		
		System.out.println("Preço: ");
		double preco = sc.nextDouble();
						
		Produtos produtos = new Produtos(name, preco);
				
		System.out.println();
		System.out.println("Dados do produto: "+ produtos.toString());
		
		System.out.println();
		System.out.println("Digite a quantidade de produtos adicionados: ");
		int quantidade = sc.nextInt();
		produtos.addProduto(quantidade);
		
		System.out.println();
		System.out.println("Dados atualizados: "+ produtos.toString());
		
		System.out.println();
		System.out.println("Digite a quantidade de produtos vendidos: ");
		quantidade = sc.nextInt();
		produtos.removerProduto(quantidade);
		
		System.out.println();
		System.out.println("Dados atualizados: "+ produtos.toString());
		sc.close();
	}

}
