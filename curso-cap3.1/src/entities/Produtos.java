package entities;

public class Produtos {
	
	private String name;
	private double preco;
	private int quantidade;
	
	public Produtos(String name, double preco, int quantidade) {
		
		this.name = name;
		this.preco = preco;
		this.quantidade = quantidade;
	}
	
	public Produtos(String name, double preco) {
		
		this.name = name;
		this.preco = preco;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	public int getQuantidade() {
		return quantidade;
	}
	
	public double valorTotalDentroEstoque() {
		
		return preco * quantidade;
		
	}
	
	public void addProduto(int quantidade) {
				
		this.quantidade += quantidade;
				
	}
	
	public void removerProduto(int quantidade) {
		
		this.quantidade-= quantidade;
		
	}
	
	public String toString() {
		
		return name	
			+ ", R$"
			+ String.format("%.2f", preco)
			+ ", " 
			+ quantidade 
			+ " unidades, Total: R$" 
			+ String.format("%.2f", valorTotalDentroEstoque());
		
	}

}
