package application;

import device.ComboDevice;
import device.ConcretePrinter;
import device.ConcreteScanner;

public class Program {

	public static void main(String[] args) {
		
		ConcretePrinter p = new ConcretePrinter("1980");
		p.processDoc("My Letter");
		p.print("My Letter");
		
		ConcreteScanner s = new ConcreteScanner("2003");
		s.processDoc("My email");
		System.out.println("Scan result: " + s.scan());
		
		ComboDevice c = new ComboDevice("2020");
		c.processDoc("My Dissertation");
		c.print("My Dissertation");
		System.out.println("Scan result: " + c.scan());

	}

}
