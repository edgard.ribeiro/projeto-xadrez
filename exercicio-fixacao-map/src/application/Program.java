package application;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;



public class Program {

	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Insira o caminho completo do arquivo: ");
		String path = sc.nextLine();
		
		try(BufferedReader br = new BufferedReader(new FileReader(path))){
			Map<String, Integer> vot = new LinkedHashMap<>();
						
			String line = br.readLine();
			while(line != null) {
				String[] campos = line.split(",");
				String nome = campos[0];
				int voto = Integer.parseInt(campos[1]);
				
				if(vot.containsKey(nome)) {
					int votosEleicao = vot.get(nome);
					vot.put(nome, voto + votosEleicao);
					
				}else {
					vot.put(nome, voto);
				}
				
				line = br.readLine();
			}
			
			for(String ele : vot.keySet()) {
				System.out.println(ele + ": " + vot.get(ele));
			}
		}catch(IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
		
		sc.close();

	}

}
