package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import entities.EmpregadoTerceirizado;
import entities.Empregadora;

public class Program {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<Empregadora> list = new ArrayList<>();
		
		System.out.print("Digite a quantidade de funcionários: ");
		int n = sc.nextInt();
		
		for(int i = 1; i <= n; i++) {
			
			System.out.println("Dados do Empregado #" + i + ":");
			System.out.print("É terceirizado (y/n): ");
			char ch = sc.next().charAt(0);
			System.out.print("Nome: ");
			sc.nextLine();
			String nome = sc.nextLine();
			System.out.print("Horas: ");
			int horas = sc.nextInt();
			System.out.print("Valor por hora: ");
			double valorporhora = sc.nextDouble();
			if(ch == 'y') {
				System.out.print("Carga Adicional: ");
				double ca = sc.nextDouble();
				Empregadora emp = new EmpregadoTerceirizado(nome, horas, valorporhora, ca);
				list.add(emp);
			}else {
				Empregadora emp = new Empregadora(nome, horas, valorporhora);
				list.add(emp);
			}
									
		}
		
		System.out.println();
		System.out.println("Pagamentos:");
		for(Empregadora emp2 : list) {
			System.out.println(emp2.getNome() + " - R$ " + String.format("%.2f", emp2.pagamento()));
		}
		
		sc.close();
	}

}
