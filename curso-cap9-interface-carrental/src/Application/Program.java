package Application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import model.entities.CarRental;
import model.entities.Veiculo;
import model.service.BrasilTaxaServico;
import model.service.RentalServico;

public class Program {

	public static void main(String[] args) throws ParseException{
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:ss");
		
		System.out.println("Digite a data do aluguel");
		System.out.print("Modelo do Carro: ");
		String carModel = sc.nextLine();
		System.out.print("Retirada (dd/MM/yyyy hh:ss): ");
		Date start = sdf.parse(sc.nextLine());
		System.out.print("Retorno (dd/MM/yyyy hh:ss): ");
		Date finish = sdf.parse(sc.nextLine());
		
		CarRental cr = new CarRental(start, finish, new Veiculo(carModel));
		
		System.out.print("Digite o preço por hora: ");
		double precoPorHora = sc.nextDouble();
		System.out.print("Digite o preço por dia: ");
		double precoPorDia = sc.nextDouble();
		
		RentalServico rentalServico = new RentalServico(precoPorHora, precoPorDia, new BrasilTaxaServico());
		
		rentalServico.processoFatura(cr);
		
		System.out.println("FATURAMENTO:");
		System.out.println("Pagamento básico: " + String.format("%.2f", cr.getFatura().getPagamentoBasico()));
		System.out.println("Taxa: " + String.format("%.2f", cr.getFatura().getTaxa()));
		System.out.println("Pagamento Total: " + String.format("%.2f", cr.getFatura().getPagamentoTotal()));
		
		
		sc.close();

	}

}
