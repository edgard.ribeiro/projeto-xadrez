package model.service;

import model.entities.CarRental;
import model.entities.Fatura;

public class RentalServico {
	
	private Double precoPorHora;
	private Double precoPorDia;
	
	private TaxaServico taxaServico;
	
	public RentalServico() {
		
	}

	public RentalServico(Double precoPorHora, Double precoPorDia, TaxaServico taxaServico) {
		super();
		this.taxaServico = taxaServico;
		this.precoPorHora = precoPorHora;
		this.precoPorDia = precoPorDia;
	}

	public Double getPrecoPorHora() {
		return precoPorHora;
	}

	public void setPrecoPorHora(Double precoPorHora) {
		this.precoPorHora = precoPorHora;
	}

	public Double getPrecoPorDia() {
		return precoPorDia;
	}

	public void setPrecoPorDia(Double precoPorDia) {
		this.precoPorDia = precoPorDia;
	}
	
	
	public TaxaServico getTaxaServico() {
		return taxaServico;
	}

	public void setTaxaServico(TaxaServico taxaServico) {
		this.taxaServico = taxaServico;
	}

	public void processoFatura(CarRental carrental) {
		
		long t1 = carrental.getStart().getTime();
		long t2 = carrental.getFinish().getTime();
		double horas = (double)(t2 - t1) / 1000 / 60 / 60;
		
		double pagamentoBasico;
		if(horas <= 12.0) {
			pagamentoBasico = Math.ceil(horas) * precoPorHora;
		}else {
			pagamentoBasico = Math.ceil(horas / 24) * precoPorDia;
 		}
		
		double taxa = taxaServico.taxa(pagamentoBasico);
		
		carrental.setFatura(new Fatura(pagamentoBasico, taxa));
	}
	

}
