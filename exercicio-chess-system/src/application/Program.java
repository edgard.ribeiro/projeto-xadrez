package application;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import chess.ChessExcecao;
import chess.ChessPeca;
import chess.JogoChess;
import chess.PosicaoChess;

public class Program {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		JogoChess jogoChess = new JogoChess();
		List<ChessPeca> capturada = new ArrayList<>();

		while (!jogoChess.getCheckMate()) {
			try {
				UI.clearScreen();
				UI.printJogo(jogoChess, capturada);
				System.out.println();
				System.out.print("Origem: ");
				PosicaoChess origem = UI.lerPosicaoChess(sc);
				
				boolean[][] possivelMovimentos = jogoChess.movimentoPossiveis(origem);
				UI.clearScreen();
				UI.printTabuleiro(jogoChess.getPecas(), possivelMovimentos);
				System.out.println();
				System.out.print("Destino: ");
				PosicaoChess destino = UI.lerPosicaoChess(sc);

				ChessPeca pecaCapturada = jogoChess.performanceChessMovimento(origem, destino);
				
				if(pecaCapturada != null) {
					capturada.add(pecaCapturada);
				}
				
				if(jogoChess.getPromovido() != null) {
					System.out.print("Digite a peça para promoção (B/T/C/Q): ");
					String tipo = sc.nextLine().toUpperCase();
					while(!tipo.equals("B") && !tipo.equals("C") && !tipo.equals("T") && !tipo.equals("Q")) {
						System.out.print("Valor Inválido, digite novamente a peça para promoção (B/T/C/Q): ");
						tipo = sc.nextLine().toUpperCase();
					}
					jogoChess.trocarPecaPromovido(tipo);
				}
			} catch (ChessExcecao e) {
				System.out.println(e.getMessage());
				sc.nextLine();
			} catch(InputMismatchException e) {
				System.out.println(e.getMessage());
				sc.nextLine();
			} 
		}
		UI.clearScreen();
		UI.printJogo(jogoChess, capturada);
	}

}
