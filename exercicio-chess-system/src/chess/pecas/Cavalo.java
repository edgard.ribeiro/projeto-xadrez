package chess.pecas;

import boardgame.Posicao;
import boardgame.Tabuleiro;
import chess.ChessPeca;
import chess.Cor;

public class Cavalo extends ChessPeca {

	public Cavalo(Tabuleiro tabuleiro, Cor cor) {
		super(tabuleiro, cor);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "C";
	}

	private boolean possoMovimentar(Posicao posicao) {
		ChessPeca p = (ChessPeca) getTabuleiro().peca(posicao);

		return p == null || p.getCor() != getCor();
	}

	@Override
	public boolean[][] possiveisMovimentos() {
		boolean[][] mat = new boolean[getTabuleiro().getLinhas()][getTabuleiro().getColunas()];

		Posicao p = new Posicao(0, 0);

		// Acima - Esquerda
		p.setValores(posicao.getLinha() - 2, posicao.getColuna() - 1);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Abaixo - Esquerda
		p.setValores(posicao.getLinha() + 2, posicao.getColuna() - 1);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Esquerda - Acima
		p.setValores(posicao.getLinha() - 1, posicao.getColuna() - 2);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Esquerda - Abaixo
		p.setValores(posicao.getLinha() + 1, posicao.getColuna() - 2);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Direita - Acima
		p.setValores(posicao.getLinha() - 1, posicao.getColuna() + 2);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Direita - Abaixo
		p.setValores(posicao.getLinha() + 1, posicao.getColuna() + 2);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Acima - Direita
		p.setValores(posicao.getLinha() - 2, posicao.getColuna() + 1);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Abaixo - Direita
		p.setValores(posicao.getLinha() + 2, posicao.getColuna() + 1);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}
		return mat;
	}

}
