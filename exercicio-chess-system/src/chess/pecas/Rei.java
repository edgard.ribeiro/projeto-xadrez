package chess.pecas;

import boardgame.Posicao;
import boardgame.Tabuleiro;
import chess.ChessPeca;
import chess.Cor;
import chess.JogoChess;

public class Rei extends ChessPeca {

	private JogoChess jogoChess;
	
	public Rei(Tabuleiro tabuleiro, Cor cor, JogoChess jogoChess) {
		super(tabuleiro, cor);
		this.jogoChess = jogoChess;
		
	}

	@Override
	public String toString() {
		return "K";
	}

	private boolean possoMovimentar(Posicao posicao) {
		ChessPeca p = (ChessPeca) getTabuleiro().peca(posicao);

		return p == null || p.getCor() != getCor();
	}
	
	private boolean testeTorreRock(Posicao posicao) {
		ChessPeca p = (ChessPeca)getTabuleiro().peca(posicao);
		return p != null && p instanceof Torre && p.getCor() == getCor() && p.getContadorMovimento() == 0;
	}

	@Override
	public boolean[][] possiveisMovimentos() {
		boolean[][] mat = new boolean[getTabuleiro().getLinhas()][getTabuleiro().getColunas()];

		Posicao p = new Posicao(0, 0);

		// Acima
		p.setValores(posicao.getLinha() - 1, posicao.getColuna());

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Abaixo
		p.setValores(posicao.getLinha() + 1, posicao.getColuna());

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Esquerda
		p.setValores(posicao.getLinha(), posicao.getColuna() - 1);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Direita
		p.setValores(posicao.getLinha(), posicao.getColuna() + 1);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Diagonal - Esquerda - Acima
		p.setValores(posicao.getLinha() - 1, posicao.getColuna() - 1);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Diagonal - Esquerda - Abaixo
		p.setValores(posicao.getLinha() + 1, posicao.getColuna() - 1);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Diagonal - Direita - Acima
		p.setValores(posicao.getLinha() - 1, posicao.getColuna() + 1);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}

		// Diagonal - Direita - Abaixo
		p.setValores(posicao.getLinha() + 1, posicao.getColuna() + 1);

		if (getTabuleiro().posicaoExiste(p) && possoMovimentar(p)) {
			mat[p.getLinha()][p.getColuna()] = true;
		}
		
		//Movimento especial Rock
		if(getContadorMovimento() == 0 && !jogoChess.getCheck()) {
			//Movimento especial Rock pequeno
			Posicao posT1 = new Posicao(posicao.getLinha(), posicao.getColuna() + 3);
			if(testeTorreRock(posT1)) {
				Posicao p1 = new Posicao(posicao.getLinha(), posicao.getColuna() + 1);
				Posicao p2 = new Posicao(posicao.getLinha(), posicao.getColuna() + 2);
				if(getTabuleiro().peca(p1) == null && getTabuleiro().peca(p2) == null) {
					mat[posicao.getLinha()][posicao.getColuna() + 2] = true;
				}
			}
			//Movimento especial Rock grande
			Posicao posT2 = new Posicao(posicao.getLinha(), posicao.getColuna() - 4);
			if(testeTorreRock(posT2)) {
				Posicao p1 = new Posicao(posicao.getLinha(), posicao.getColuna() - 1);
				Posicao p2 = new Posicao(posicao.getLinha(), posicao.getColuna() - 2);
				Posicao p3 = new Posicao(posicao.getLinha(), posicao.getColuna() - 3);
				if(getTabuleiro().peca(p1) == null && getTabuleiro().peca(p2) == null && getTabuleiro().peca(p3) == null) {
					mat[posicao.getLinha()][posicao.getColuna() - 2] = true;
				}
			}
		}

		return mat;
	}

}
