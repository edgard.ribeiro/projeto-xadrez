package chess;

import boardgame.Posicao;

public class PosicaoChess {
	
	private int linha;
	private char coluna;
	
	public PosicaoChess(char coluna, int linha) {
		if(coluna < 'a' || coluna > 'h' || linha < 1 || linha > 8) {
			throw new ChessExcecao("Erro instanciando PosicaoChess. Valores válidos são da posição a1 até h8.");
		}
		this.linha = linha;
		this.coluna = coluna;
	}

	public int getLinha() {
		return linha;
	}
	
	public char getColuna() {
		return coluna;
	}

	protected Posicao toPosicao() {
		return new Posicao(8 - linha, coluna - 'a');
	}
	
	protected static PosicaoChess fromPosicao(Posicao posicao) {
		return new PosicaoChess((char)('a' + posicao.getColuna()), 8 - posicao.getLinha());
		
	}
	
	@Override
	public String toString() {
		return "" + coluna + linha;
		
	}

}
