package chess;

import boardgame.Pecas;
import boardgame.Posicao;
import boardgame.Tabuleiro;

public abstract class ChessPeca extends Pecas{
	
	private Cor cor;
	private int contadorMovimento;
		
	public ChessPeca(Tabuleiro tabuleiro, Cor cor) {
		super(tabuleiro);
		this.cor = cor;
		
	}

	public Cor getCor() {
		return cor;
	}
		
	public int getContadorMovimento() {
		return contadorMovimento;
	}

	public void aumentarContagemMove() {
		contadorMovimento++;
	}
	
	public void diminuirContagemMove() {
		contadorMovimento--;
	}

	public PosicaoChess getPosicaoChess() {
		return PosicaoChess.fromPosicao(posicao);
	}
	protected boolean haPecaOponente(Posicao posicao) {
		ChessPeca p = (ChessPeca)getTabuleiro().peca(posicao);
		return p != null && p.getCor() != cor;
	}
}
