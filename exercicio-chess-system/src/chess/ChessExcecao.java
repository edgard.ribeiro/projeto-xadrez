package chess;

import boardgame.TabuleiroExcecao;

public class ChessExcecao extends TabuleiroExcecao{

	private static final long serialVersionUID = 1L;
	
	public ChessExcecao(String msg) {
		super(msg);
	}

}
