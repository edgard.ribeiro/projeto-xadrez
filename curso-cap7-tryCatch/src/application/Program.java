package application;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		
		metodo1();
		System.out.println("Teste finalizado");
	}
	
	public static void metodo1() {
		System.out.println("****INICIANDO METODO 1****");
		metodo2();
		System.out.println("****FINALIZANDO METODO 1****");
	}
	
	public static void metodo2() {
		System.out.println("****INICIANDO METODO 2****");
		Scanner sc = new Scanner(System.in);

		try {
			String[] vect = sc.nextLine().split(" ");
			int position = sc.nextInt();
			
			System.out.println(vect[position]);
			}catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("posição invalida");
				e.printStackTrace();
				sc.next();
			}catch (InputMismatchException e) {
				System.out.println("Letra inexistente, por favor coloque numero.");
				
			}
		sc.close();		
		System.out.println("****FINALIZANDO O METODO 2****");

	}

}
