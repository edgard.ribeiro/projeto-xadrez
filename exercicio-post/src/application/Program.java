package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import entities.Comente;
import entities.Post;

public class Program {

	public static void main(String[] args) throws ParseException {
				
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		Comente c1 = new Comente("Tenha uma boa viagem");
		Comente c2 = new Comente("Mulher isso é incrível!");
		
		Post p1 = new Post(sdf.parse("21/06/2018 13:05:44"), "Viajando para Nova Zelândia", "Eu quero visitar essa cidade Maravilhosa!", 12);
		
		p1.adicionarComentario(c1);
		p1.adicionarComentario(c2);
		
		Comente c3 = new Comente("Boa noite!");
		Comente c4 = new Comente("Que a força esteja com você!");
		
		Post p2 = new Post(sdf.parse("28/76/2018 23:15:44"), "Boa noite galera", "Vejo vocês amanhã", 5);
		
		p2.adicionarComentario(c3);
		p2.adicionarComentario(c4);
		
		System.out.println(p1);
		System.out.println();
		System.out.println(p2);
	}
	

}
