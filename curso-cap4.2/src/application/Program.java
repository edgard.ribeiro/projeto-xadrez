package application;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Program {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		
		list.add("Edgard");
		list.add("Amanda");
		list.add("Sophie");
		list.add("Bob");
		list.add(2, "Silvia");
		list.add(0, "Elinete");
				
		System.out.println(list.size());
		
		for(String x : list) {
			System.out.println(x);
		}
		System.out.println("-----------------------");
		list.remove(4);
		list.removeIf(x -> x.charAt(0) == 'S');
		
		System.out.println(list.size());
		
		for(String x : list) {
			System.out.println(x);
		}
		System.out.println("-----------------------");
		System.out.println("Index of Amanda: " + list.indexOf("Amanda"));
		System.out.println("Index of Sophie: " + list.indexOf("Sophie"));
		System.out.println("-----------------------");
		List<String> result = list.stream().filter(x -> x.charAt(0) == 'E').collect(Collectors.toList());
		
		for(String x : result) {
			System.out.println(x);
		}
		System.out.println("-----------------------");
		String name = list.stream().filter(x -> x.charAt(0) == 'E').findFirst().orElse(null);

		System.out.println(name);
	}

}
