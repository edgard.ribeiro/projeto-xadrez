package Application;


import model.entities.AbstractShape;
import model.entities.Circle;
import model.entities.Retangle;
import model.enun.Color;

public class Program {

	public static void main(String[] args) {
		
		AbstractShape ac1 = new Circle(Color.PRETO, 2.0);
		AbstractShape ac2 = new Retangle(Color.AZUL, 3.0, 4.0);
		
		System.out.println("Circle color: " + ac1.getColor());
		System.out.println("Circle area: " + String.format("%.3f", ac1.area()));
		System.out.println("Retangle color: " + ac2.getColor());
		System.out.println("Retangle area: " + String.format("%.3f", ac2.area()));
	}
}
