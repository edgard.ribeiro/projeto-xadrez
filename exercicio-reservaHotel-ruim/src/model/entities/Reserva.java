package model.entities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Reserva {
	
	private Integer quartoNumero;
	private Date checkin;
	private Date checkout;
	
	public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public Reserva() {
		
	}

	public Reserva(Integer quartoNumero, Date checkin, Date checkout) {
		
		this.quartoNumero = quartoNumero;
		this.checkin = checkin;
		this.checkout = checkout;
	}

	public Integer getQuartoNumero() {
		return quartoNumero;
	}

	public void setQuartoNumero(Integer quartoNumero) {
		this.quartoNumero = quartoNumero;
	}

	public Date getCheckin() {
		return checkin;
	}
	
	public Date getCheckout() {
		return checkout;
	}

	public long duracao() {
		long diff = checkout.getTime() - checkin.getTime();
		
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	public String atualizarData(Date checkin, Date checkout) {
		
		Date now = new Date();
		if(checkin.before(now) || checkout.before(now)) {
			return "Erro na reserva: As datas de reserva para atualização, devem ser futuras.";
		}
		if(!checkout.after(checkin)) {
			return "Erro na reserva: A data de check-out deve ser posterior à data de check-in";
		}
		
		this.checkin = checkin;
		this.checkout = checkout;
		
		return null;
		
	}
	@Override
	public String toString() {
				
		return "Quarto: "
			+ quartoNumero
			+", check-in: "
			+ sdf.format(checkin)
			+ ", check-out: "
			+ sdf.format(checkout)
			+ ", "
			+ duracao()
			+ " noites";
	}
	

}
