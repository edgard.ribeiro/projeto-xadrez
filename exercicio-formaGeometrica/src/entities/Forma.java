package entities;

import entitiesenums.Color;

public abstract class Forma {
	
	private Color cor;
	
	public Forma() {
		
	}
	
	public Forma(Color cor) {
	
		this.cor = cor;
	}



	public Color getCor() {
		return cor;
	}

	public void setCor(Color cor) {
		this.cor = cor;
	}
	
	public abstract double area();
		
	
}
