package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import model.entities.Reserva;
import model.exceptions.DominioExcecao;

public class Program {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		try {
			System.out.print("Numero do Quarto: ");
			int numero = sc.nextInt();
			System.out.print("Data do check-in (dd/MM/yyyy): ");
			Date checkin = sdf.parse(sc.next());
			System.out.print("Data do check-out (dd/MM/yyyy): ");
			Date checkout = sdf.parse(sc.next());
	
			Reserva reserva = new Reserva(numero, checkin, checkout);
			System.out.println("Reserva: " + reserva.toString());
	
			System.out.println();
			System.out.println("Atualize a data de reserva:");
			System.out.print("Data do check-in (dd/MM/yyyy): ");
			checkin = sdf.parse(sc.next());
			System.out.print("Data do check-out (dd/MM/yyyy): ");
			checkout = sdf.parse(sc.next());
	
			reserva.atualizarData(checkin, checkout);
			System.out.println("Reserva: " + reserva.toString());
			
		}catch (ParseException e) {
			System.out.println("Formato de data inválida!");
		}catch(DominioExcecao e){
			System.out.println(e.getMessage());
		}catch(RuntimeException e){
			System.out.println("Erro inesperado!");
		}

		sc.close();

	}

}
