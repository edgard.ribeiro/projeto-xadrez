package application;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.stream.Collectors;

import entities.Product;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
						
		System.out.print("Insira o caminho completo do arquivo: ");
		String path = sc.nextLine();
		
		try(BufferedReader br = new BufferedReader(new FileReader(path))){
			
			List<Product> list = new ArrayList<>();
			
			String line = br.readLine();
			while(line != null) {
				String campos[] = line.split(",");
				list.add(new Product(campos[0], Double.parseDouble(campos[1])));
				line = br.readLine();
			}
			
			double media = list.stream()
					.map(p -> p.getPreco())
					.reduce(0.0, (x, y) -> x + y) / list.size();
			System.out.println("A média do preço: " + String.format("%.2f", media));
			
			Comparator<String> comp = (s1, s2) -> s1.toUpperCase().compareTo(s2.toUpperCase());
			
			List<String> name = list.stream()
					.filter(p -> p.getPreco() < media)
					.map(p -> p.getNome())
					.sorted(comp.reversed())
					.collect(Collectors.toList());
			for(String n : name) {
				System.out.println(n);

			}
			
		}catch(IOException e){
			System.out.println("Error: " + e.getMessage());
		}
		
		sc.close();

	}

}
