package application;

import java.util.Locale;
import java.util.Scanner;

import entities.Conta;

public class Program {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		Conta conta;
				
		System.out.println("Bem Vindo ao Banco Edraziel");
		
		System.out.println("Digite seu Nome: ");
		String nome = sc.nextLine();
		
		System.out.println("Digite sua Agência: ");
		int numeroAgencia = sc.nextInt();
		
		System.out.println("Digite sua Conta: ");
		int numeroConta = sc.nextInt();
		
		System.out.println("Voce quer abrir a conta com deposito inicial (y/n)? ");
		char response = sc.next().charAt(0);
		
		if(response == 'y') {
			System.out.println("Insira o valor do Depósito Inicial: ");
			double depositoInicial = sc.nextDouble();
			conta = new Conta(nome, numeroAgencia, numeroConta, depositoInicial);
		}else {
			conta = new Conta(nome, numeroAgencia, numeroConta);
		}		
				
		System.out.println();
		System.out.println("Dados da Conta: " + conta.toString());
		
		System.out.println();
		System.out.println("Insira o valor do Depósito: ");
		double saldo = sc.nextDouble();
		conta.deposito(saldo);
		
		System.out.println();
		System.out.println("Dados da conta atualizado: " + conta.toString());
		
		System.out.println();
		System.out.println("Insira o valor do Saque: ");
		saldo = sc.nextDouble();
		conta.saque(saldo);
		
		System.out.println();
		System.out.println("Dados da conta atualizado: " + conta.toString());
		
		sc.close();
	}

}
