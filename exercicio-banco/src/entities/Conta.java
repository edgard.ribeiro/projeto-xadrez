package entities;

public class Conta {
	
	private String nome;
	private int numeroAgencia;
	private int numeroConta;
	private double saldo;
	
	public Conta(String nome, int numeroAgencia, int numeroConta, double depositoInicial) {
		
		this.nome = nome;
		this.numeroAgencia = numeroAgencia;
		this.numeroConta = numeroConta;
		deposito(depositoInicial);
	}
	
	public Conta(String nome, int numeroAgencia, int numeroConta) {
		
		this.nome = nome;
		this.numeroAgencia = numeroAgencia;
		this.numeroConta = numeroConta;
		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
		
	public int getNumeroAgencia() {
		return numeroAgencia;
	}
	
	public int getNumeroConta() {
		return numeroConta;
	}

	public double getSaldo() {
		return saldo;
	}
		
	public boolean deposito(double valor) {
		if(valor > 0) {
			this.saldo += valor;
			return(true);
		}else {
			return(false);
		}
		
	}
	
	public boolean saque(double valor) {
		if(valor > 0) {
			this.saldo -= valor + 5.0;
			return(true);
		}else {
			System.out.println("Saldo Indisponível");
			return(false);
		}
				
	}
	
	public String toString() {
		
		return nome
			+ ", Agência: "
			+ numeroAgencia
			+ ", Conta: "
			+ numeroConta
			+ ", saldo: R$ "
			+ String.format("%.2f", saldo);
		
	}
	

}
