package application;

import java.util.Locale;
import java.util.Scanner;

import entities.Quartos;

public class Program {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
				
		Quartos[] aluguel = new Quartos[10];
		
		System.out.println("Quantos quartos voce quer alugar? ");
		
		int n = sc.nextInt();
		for(int i = 1; i <= n; i++) {
			System.out.println();
			System.out.println("Aluguel #" + i + ":");
			System.out.println("Nome: ");
			sc.nextLine();
			String nome = sc.nextLine();
			System.out.println("Email: ");
			String email = sc.nextLine();
			System.out.println("Quarto: ");
			int quarto = sc.nextInt();
			aluguel[quarto] = new Quartos(nome, email);
			
		}
		
		System.out.println();
		System.out.println("Quartos Ocupados: ");
		for(int i = 0; i < 10; i++) {
			if(aluguel[i] != null) {
				System.out.println(i + ": " + aluguel[i]);
			}
		}
		sc.close();
	}

}
