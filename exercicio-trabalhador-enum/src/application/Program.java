package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import entidade.Departamento;
import entidade.HoraContrato;
import entidade.Trabalhador;
import entidade.nums.NivelTrabalhador;

public class Program {

	public static void main(String[] args) throws ParseException {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		System.out.print("Digite o nome do Departamento: ");
		String nomeDepartamento = sc.nextLine();
		System.out.println();
		System.out.println("Digite dados do Trabalhador:");
		System.out.print("Nome: ");
		String nome = sc.nextLine();
		System.out.print("Nivel: ");
		String nivelTrabalhador = sc.nextLine();
		System.out.print("Salário Base: ");
		double salariobase = sc.nextDouble();
		Trabalhador trabalhador = new Trabalhador(nome, NivelTrabalhador.valueOf(nivelTrabalhador), salariobase, new Departamento(nomeDepartamento)); 
		
		System.out.print("Quantos contratos para este trabalhador? ");
		int contrat = sc.nextInt();
		
		for(int i = 1; i <= contrat; i++) {
			System.out.println("Digite os dados do contrato #" + i + ":");
			System.out.print("Data (DD/MM/YYYY): ");
			Date dataContrato = sdf.parse(sc.next());
			System.out.print("Valor por Hora: ");
			Double valorPorHora = sc.nextDouble();
			System.out.print("Duração (Hora): ");
			Integer duracaoHora = sc.nextInt();
			HoraContrato contrato = new HoraContrato(dataContrato, valorPorHora, duracaoHora);
			trabalhador.adicionarContrato(contrato);
		}
		System.out.println();
		System.out.print("Digite o mês e o ano para calcular a renda (MM/YYYY): ");
		String mesEAno = sc.next();
		int mes = Integer.parseInt(mesEAno.substring(0, 2));
		int ano = Integer.parseInt(mesEAno.substring(3));
		System.out.println("Nome: " + trabalhador.getNome());
		System.out.println("Departamento: " + trabalhador.getDepartamento().getNome());
		System.out.println("Renda para " + mesEAno + ": " + String.format("%.2f", trabalhador.renda(ano, mes)));
		
		sc.close();
	}

}
