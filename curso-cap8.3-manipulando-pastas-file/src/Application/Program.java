package Application;

import java.io.File;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a folder path: ");
		String strpath = sc.nextLine();
		
		File path = new File(strpath);
		
		File[] folders = path.listFiles(File::isDirectory);
		System.out.println("Foders:");
		for(File folder : folders) {
			System.out.println(folder);
		}
		
		File[] files = path.listFiles(File::isFile);
		System.out.println("Files:");
		for(File file : files) {
			System.out.println(file);
		}
		
		boolean sucesso = new File(strpath + "/subdir").mkdir();
		System.out.println("Diretório criado com sucesso: " + sucesso);
		
		sc.close();
	}

}
