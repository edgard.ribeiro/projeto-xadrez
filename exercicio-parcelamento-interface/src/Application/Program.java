package Application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import model.entities.Contrato;
import model.entities.Parcela;
import model.servico.ContratoServico;
import model.servico.PaypalServico;

public class Program {

	public static void main(String[] args) throws ParseException {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		System.out.println("Digite os dados do Contrato:");
		System.out.print("Número: ");
		int numero = sc.nextInt();
		System.out.print("Data (dd/MM/yyyy): ");
		Date data = sdf.parse(sc.next());
		System.out.print("Valor do contrato: ");
		double valor = sc.nextDouble();
				
		Contrato contrato = new Contrato(numero, data, valor);
		
		System.out.print("Digite o numero de parcelas: ");
		int parcela = sc.nextInt();
		
		ContratoServico contratoServico = new ContratoServico(new PaypalServico());
		
		contratoServico.processoContrato(contrato, parcela);
		
		System.out.println("Parcelas:");
		for(Parcela x : contrato.getParcelas()) {
			System.out.println(x);
		}
				
				
		sc.close();		
	}

}
