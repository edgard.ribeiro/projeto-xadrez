package model.servico;

public class PaypalServico implements PagamentoOnlineServico{
	
	private static final double TAXA_PORCENTAGEM = 0.02;
	private static final double INTERESSE_MENSAL = 0.01;
	
	@Override
	public Double pagamentoTaxa(double montante) {
		
		return montante * TAXA_PORCENTAGEM;	
	}
	
	@Override
	public Double interesse(double montante, int mes) {
		
		return montante * INTERESSE_MENSAL * mes;
	}

}
