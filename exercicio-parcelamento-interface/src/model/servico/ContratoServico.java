package model.servico;

import java.util.Calendar;
import java.util.Date;

import model.entities.Contrato;
import model.entities.Parcela;

public class ContratoServico {
	
	private PagamentoOnlineServico pagamentoOnlineServico;
			
	public ContratoServico(PagamentoOnlineServico pagamentoOnlineServico) {
		super();
		this.pagamentoOnlineServico = pagamentoOnlineServico;
	}

	public void processoContrato(Contrato contrato, int mes) {
		
		double quotaBasica = contrato.getValorTotal() / mes;
		for(int i = 1; i <= mes; i++) {
			Date data = addMes(contrato.getData(), i);
			double updateQuota = quotaBasica + pagamentoOnlineServico.interesse(quotaBasica, i);
			double fullQuota = updateQuota + pagamentoOnlineServico.pagamentoTaxa(updateQuota);
			contrato.addParcela(new Parcela(data, fullQuota));
		}
		
	}

	private Date addMes(Date data, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.add(Calendar.MONTH, n);
		
		return cal.getTime();
	}

}
