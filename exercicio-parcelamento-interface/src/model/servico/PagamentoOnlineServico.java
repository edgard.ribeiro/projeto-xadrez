package model.servico;

public interface PagamentoOnlineServico {
	
	public Double pagamentoTaxa(double montante);
	public Double interesse(double montante, int mes);

}
