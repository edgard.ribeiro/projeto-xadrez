package application;

import java.util.Locale;
import java.util.Scanner;

import service.ServicoTaxa;
import service.ServicoTaxaBrasil;
import service.ServicoTaxaUsa;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Emprestimo: ");
		double montante = sc.nextDouble();
		System.out.print("Mês: ");
		int mes = sc.nextInt();
		
		//ServicoTaxa is = new ServicoTaxaUsa(1.0);
		ServicoTaxa is = new ServicoTaxaBrasil(2.0);
		double pagamento = is.pagamento(montante, mes);
		
		System.out.println("Pagamento depois de " + mes + " meses:");
		System.out.println(String.format("%.2f", pagamento));
		
		sc.close();

	}

}
