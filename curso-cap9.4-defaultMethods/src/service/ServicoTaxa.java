package service;

import java.security.InvalidParameterException;

public interface ServicoTaxa {
	
	double getTaxa();
	default double pagamento(double montante, int meses) {
		if(meses < 1) {
			throw new InvalidParameterException("Os meses devem ser maiores que zero");
		}
		return montante * Math.pow(1.0 + getTaxa() / 100.0, meses);
	}

}
