package service;

public class ServicoTaxaBrasil implements ServicoTaxa{
	
	private Double taxa;

	public ServicoTaxaBrasil(Double taxa) {
		
		this.taxa = taxa;
	}
	
	@Override
	public double getTaxa() {
		return taxa;
	}
	
}
