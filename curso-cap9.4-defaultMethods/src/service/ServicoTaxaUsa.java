package service;

public class ServicoTaxaUsa implements ServicoTaxa{
	
	private Double taxa;

	public ServicoTaxaUsa(Double taxa) {
		
		this.taxa = taxa;
	}

	@Override
	public double getTaxa() {
		return taxa;
	}
	
	
}
