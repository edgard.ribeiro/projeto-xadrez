package entities;

public abstract class Conta {
	
	private Integer agencia;
	private Integer nconta;
	private String titular;
	protected Double saldo;
	
	public Conta() {
		
		
		
	}

	public Conta(Integer agencia, Integer nconta, String titular, Double saldo) {
		
		this.agencia = agencia;
		this.nconta = nconta;
		this.titular = titular;
		this.saldo = saldo;
	}

	public Integer getAgencia() {
		return agencia;
	}
	
	public Integer getNconta() {
		return nconta;
	}
	
	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public Double getSaldo() {
		return saldo;
	}
	
	public void deposito(Double valor) {
		if(valor > 0) {
			this.saldo += valor;
		}
	}

	public void saque(Double valor) {
		if (valor > 0) {
			this.saldo -= valor + 5.0;
		}
	}
		
}
