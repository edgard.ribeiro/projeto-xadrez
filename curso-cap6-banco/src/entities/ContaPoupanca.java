package entities;

public class ContaPoupanca extends Conta{
	
	private Double taxaDeJuros;
	
	public ContaPoupanca() {
		super();
	}

	public ContaPoupanca(Integer agencia, Integer nconta, String titular, Double saldo, Double taxaDeJuros) {
		super(agencia, nconta, titular, saldo);
		this.taxaDeJuros = taxaDeJuros;
	}

	public Double getTaxaDeJuros() {
		return taxaDeJuros;
	}

	public void setTaxaDeJuros(Double taxaDeJuros) {
		this.taxaDeJuros = taxaDeJuros;
	}
	
	public void atualizarSaldo() {
		saldo += saldo * taxaDeJuros;
	}
	
	@Override
	public void saque(Double valor) {
		if(valor > 0) {
			this.saldo -= valor;
		}
	}

}
