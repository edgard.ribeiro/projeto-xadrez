package entities;

public class ContaEmpresa extends Conta{
	
	private Double limiteEmprestimo;
	
	public ContaEmpresa() {
		super();
	}

	public ContaEmpresa(Integer agencia, Integer nconta, String titular, Double saldo, Double limiteEmprestimo) {
		super(agencia, nconta, titular, saldo);
		this.limiteEmprestimo = limiteEmprestimo;
	}

	public Double getLimiteEmprestimo() {
		return limiteEmprestimo;
	}
	
	public void emprestimo(Double valor) {
		if(valor <= limiteEmprestimo) {
			saldo += valor - 10.0;
		}
	}
	
	@Override
	public void saque(Double valor) {
		super.saque(valor);
		saldo -= 2.0;
	}

}
