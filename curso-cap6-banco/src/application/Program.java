package application;

import java.util.ArrayList;
import java.util.List;

import entities.Conta;
import entities.ContaEmpresa;
import entities.ContaPoupanca;

public class Program {

	private static final String Conta = null;

	public static void main(String[] args) {
		
		List<Conta> list = new ArrayList<>();
		
		list.add(new ContaPoupanca(3477, 234563, "Amanda", 1000.00, 0.01));
		list.add(new ContaEmpresa(3477, 1288248, "BBTS", 1000.0, 1000.0));
		
				
		double soma = 0.0;
		for(Conta acc : list) {
			soma += acc.getSaldo();
			
		}
		
		for(Conta acc : list) {
			acc.deposito(10.0);
		}
		
		for(Conta acc : list) {
			System.out.printf("Saldo da Conta %d: %.2f%n", acc.getNconta(), acc.getSaldo());
		}
						
		System.out.printf("Total de saldo: %.2f%n", soma);
		
		
		
		
		/*UPCASTING
		
		Conta conta1 = ce;
		Conta conta2 = new ContaEmpresa(3307, 123456, "AGU", 0.0, 2200.00);
		Conta conta3 = new ContaPoupanca(3475, 128245, "Amanda", 0.0, 0.01);
		
		//DOWNCASTING
		
		ContaEmpresa conta4 = (ContaEmpresa)conta2;
		conta4.emprestimo(350.00);
				
		//ContaEmpresa conta5 = (ContaEmpresa)conta3;
		if(conta3 instanceof ContaEmpresa) {
			ContaEmpresa conta5 = (ContaEmpresa)conta3;
			conta5.emprestimo(230.00);
			System.out.println("Emprestimo Efetuado!");
		}else {
			if(conta3 instanceof ContaPoupanca) {
				ContaPoupanca conta5 = (ContaPoupanca)conta3;
				conta5.atualizarSaldo();
				System.out.println("Saldo Atualizado!");
			}else {
				System.out.println("Conta não instanciada.");
			}
		}*/
		
		
		
		
	}

	private static entities.Conta list(String conta2) {
		// TODO Auto-generated method stub
		return null;
	}

}
