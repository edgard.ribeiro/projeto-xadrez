package application;

import java.util.ArrayList;
import java.util.List;

import entities.Produto;

public class Program {

	public static void main(String[] args) {
		
		List<Produto> list = new ArrayList<>();
		
		list.add(new Produto("TV", 900.00));
		list.add(new Produto("Notebook", 1900.00));
		list.add(new Produto("Tablet", 400.00));
		
		//Metodo de Comparação 1 usando a classe MyComparator
		//Metodo de Comparação 2 usando a classe anônima chamada Comparator
		//Metodo de Comparação 3 usando expressão lâmbida dentro do list.sort(mais fácil)
		
			//Metodo de comparação 2 prescisa desse Override.
			/*@Override
			public int compare(Produto p1, Produto p2) {
				
				return p1.getNome().toUpperCase().compareTo(p2.getNome().toUpperCase());
			}*/
			
		
		
		list.sort((p1, p2) -> p1.getNome().toUpperCase().compareTo(p2.getNome().toUpperCase()));
		
		for(Produto p : list) {
			System.out.println(p);
		}

	}

}
