package application;

import java.util.Locale;
import java.util.Scanner;

import entities.Conta;
import excecoes.DominioExcecao;

public class Program {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		
		System.out.println("Bem Vindo ao Banco Raziel");

		System.out.print("Digite seu Nome: ");
		String nome = sc.nextLine();

		System.out.print("Digite sua Agência: ");
		int numeroAgencia = sc.nextInt();

		System.out.print("Digite sua Conta: ");
		int numeroConta = sc.nextInt();

		System.out.print("Saldo inicial: ");
		double saldo = sc.nextDouble();

		System.out.print("Limite de saque: ");
		double limite = sc.nextDouble();
		
		Conta conta = new Conta(numeroAgencia, numeroConta, nome, saldo, limite);
		
		System.out.println();
		System.out.print("Digite o valor do saque: ");
		double saque = sc.nextDouble();
		
		try {
			conta.saque(saque);
			System.out.println("Novo saldo: " + String.format("%.2f", conta.getSaldo()));
		}catch(DominioExcecao e) {
			System.out.println("Saque erro: " + e.getMessage());
		}

		sc.close();

	}

}
