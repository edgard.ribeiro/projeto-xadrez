package entities;

import excecoes.DominioExcecao;

public class Conta {
	
	private Integer agencia;
	private Integer nconta;
	private String titular;
	protected Double saldo;
	protected Double saqueLimite;
		
	public Conta() {
						
	}

	public Conta(Integer agencia, Integer nconta, String titular, Double saldo, Double saqueLimite) {
						
		this.agencia = agencia;
		this.nconta = nconta;
		this.titular = titular;
		this.saldo = saldo;
		this.saqueLimite = saqueLimite;
	}

	public Integer getAgencia() {
		return agencia;
	}
	
	public Integer getNconta() {
		return nconta;
	}
	
	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public Double getSaldo() {
		return saldo;
	}
			
	public Double getSaqueLimite() {
						
		return saqueLimite;
	}
		
	public void setSaqueLimite(Double saqueLimite) {
		this.saqueLimite = saqueLimite;
	}

	public void deposito(Double valor) {
		if(valor > 3000) {
			throw new DominioExcecao("Limite de deposito ultrapassado.");
		}else {
			this.saldo += valor;
		}
	}

	public void saque(Double valor) {
		if (valor > saqueLimite) {
			throw new DominioExcecao("Valor limite do saque excedido.");
		}
		if(valor > saldo) {
			throw new DominioExcecao("Saldo Insuficiente");			
		}else {
			this.saldo -= valor;
		}
	}
		


}
