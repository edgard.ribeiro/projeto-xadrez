package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import entities.Empregado;

public class Program {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<Empregado> list = new ArrayList<>();
		
		System.out.println("Quantos funcionários são registrados? ");
		
		int n = sc.nextInt();
		for(int i = 1; i <= n; i++) {
			System.out.println();
			System.out.println("Empregado #" + i + ":");
			System.out.println("Id: ");
			Integer id = sc.nextInt();
			while(temId(list, id)) {
				System.out.println("Registro de ID já existe, tente novamente: ");
				id = sc.nextInt();
			}
			System.out.println("Nome: ");
			sc.nextLine();
			String nome = sc.nextLine();
			System.out.println("Salário: ");
			Double salario = sc.nextDouble();
			Empregado empregado = new Empregado(id, nome, salario);
			
			list.add(empregado);
			
		}
		
		System.out.println("Digite o ID do funcionário que terá aumento salarial: ");
		int idSalario = sc.nextInt();
		
		Empregado empregado = list.stream().filter(x -> x.getId() == idSalario).findFirst().orElse(null);
		//Integer pos = temId(list, idSalario);
		if(empregado == null) {
			System.out.println("Não existe registro desse ID");
		}else {
			System.out.println("Digite a porcentagem: ");
			double porcento = sc.nextDouble();
			empregado.aumentarSalario(porcento);
		}
		
		System.out.println();
		System.out.println("Lista de Empregados");
		System.out.println();
		for(Empregado emp : list) {
			System.out.println(emp);
		}
				
		
		sc.close();
	}
	
	public static Integer posicao(List<Empregado> list, int id) {
		for(int i = 0; i < list.size(); i++) {
			if(list.get(i).getId() == id) {
				return i;
			}
		}
		return null;
	}
	
	public static boolean temId(List<Empregado> list, int id) {
		Empregado empregado = list.stream().filter(x -> x.getId() == id).findFirst().orElse(null);
		return empregado != null;
	}
}
