package application;

import java.util.Set;
import java.util.TreeSet;

import entities.Produto;

public class Program {

	public static void main(String[] args) {
		
		//Testa igualdade
		//Set<Produto> set = new HashSet<>();
		
		//Compara elementos
		Set<Produto> set = new TreeSet<>();
		set.add(new Produto("TV", 900.00));
		set.add(new Produto("Notebook", 1900.00));
		set.add(new Produto("Tablet", 400.00));
		
		//Também testa igualdade
		/*Produto prod = new Produto("Notebook", 1900.00);
		
		System.out.println(set.contains(prod));*/
		
		//Também compara elementos
		for(Produto prod : set) {
			System.out.println(prod);
		}

	}

}
