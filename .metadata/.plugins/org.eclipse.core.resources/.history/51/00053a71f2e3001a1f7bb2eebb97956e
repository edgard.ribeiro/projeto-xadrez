package chess;

import boardgame.Posicao;
import boardgame.Tabuleiro;
import chess.pecas.Bispo;
import chess.pecas.Cavalo;
import chess.pecas.Rainha;
import chess.pecas.Rei;
import chess.pecas.Torre;

public class JogoChess {
	
	private Tabuleiro tabuleiro;
	private int turno;
	private Cor atualJogador;
	private boolean check;
	private boolean checkMate;
	private ChessPeca enPassanteVulneravel;
	private ChessPeca promovido;
	
	public JogoChess() {
		tabuleiro = new Tabuleiro(8, 8);
		iniciarPartida();
	}
	
	public JogoChess(Tabuleiro tabuleiro, int turno, Cor atualJogador, boolean check, boolean checkMate, ChessPeca enPassanteVulneravel,
			ChessPeca promovido) {
		
		this.tabuleiro = tabuleiro;
		this.turno = turno;
		this.atualJogador = atualJogador;
		this.check = check;
		this.checkMate = checkMate;
		this.enPassanteVulneravel = enPassanteVulneravel;
		this.promovido = promovido;
	}
	
	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}

	public void setTabuleiro(Tabuleiro tabuleiro) {
		this.tabuleiro = tabuleiro;
	}

	public int getTurno() {
		return turno;
	}
	
	public void setTurno(int turno) {
		this.turno = turno;
	}
	
	public Cor getAtualJogador() {
		return atualJogador;
	}
	
	public void setAtualJogador(Cor atualJogador) {
		this.atualJogador = atualJogador;
	}
	
	public boolean isCheck() {
		return check;
	}
	
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	public boolean isCheckMate() {
		return checkMate;
	}
	
	public void setCheckMate(boolean checkMate) {
		this.checkMate = checkMate;
	}
	
	public ChessPeca getEnPassanteVulneravel() {
		return enPassanteVulneravel;
	}
	
	public void setEnPassanteVulneravel(ChessPeca enPassanteVulneravel) {
		this.enPassanteVulneravel = enPassanteVulneravel;
	}
	
	public ChessPeca getPromovido() {
		return promovido;
	}
	
	public void setPromovido(ChessPeca promovido) {
		this.promovido = promovido;
	}
	
	public ChessPeca[][] getPecas(){
		ChessPeca[][] mat = new ChessPeca[tabuleiro.getLinhas()][tabuleiro.getColunas()];
		for (int i = 0; i < tabuleiro.getLinhas(); i++) {
			for(int j = 0; j < tabuleiro.getColunas(); j++) {
				mat[i][j] = (ChessPeca) tabuleiro.peca(i, j);
			}
		}
		return mat;
	}
	
	private void iniciarPartida() {
		tabuleiro.colocarPeca(new Torre(tabuleiro, Cor.BRANCO), new Posicao(7, 0));
		tabuleiro.colocarPeca(new Cavalo(tabuleiro, Cor.BRANCO), new Posicao(7, 1));
		tabuleiro.colocarPeca(new Bispo(tabuleiro, Cor.BRANCO), new Posicao(7, 2));
		tabuleiro.colocarPeca(new Rainha(tabuleiro, Cor.BRANCO), new Posicao(7, 3));
		tabuleiro.colocarPeca(new Rei(tabuleiro, Cor.BRANCO), new Posicao(7, 4));
		tabuleiro.colocarPeca(new Bispo(tabuleiro, Cor.BRANCO), new Posicao(7, 5));
		tabuleiro.colocarPeca(new Cavalo(tabuleiro, Cor.BRANCO), new Posicao(7, 6));
		tabuleiro.colocarPeca(new Torre(tabuleiro, Cor.BRANCO), new Posicao(7, 7));
		tabuleiro.colocarPeca(new Torre(tabuleiro, Cor.PRETO), new Posicao(0, 0));
		tabuleiro.colocarPeca(new Cavalo(tabuleiro, Cor.PRETO), new Posicao(0, 1));
		tabuleiro.colocarPeca(new Bispo(tabuleiro, Cor.PRETO), new Posicao(0, 2));
		tabuleiro.colocarPeca(new Rainha(tabuleiro, Cor.PRETO), new Posicao(0, 3));
		tabuleiro.colocarPeca(new Rei(tabuleiro, Cor.PRETO), new Posicao(0, 4));
		tabuleiro.colocarPeca(new Bispo(tabuleiro, Cor.PRETO), new Posicao(0, 5));
		tabuleiro.colocarPeca(new Cavalo(tabuleiro, Cor.PRETO), new Posicao(0, 6));
		tabuleiro.colocarPeca(new Torre(tabuleiro, Cor.PRETO), new Posicao(0, 7));

	}
	
}
