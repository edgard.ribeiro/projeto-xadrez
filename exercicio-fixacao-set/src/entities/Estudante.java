package entities;

public class Estudante {
	
	private Integer estudante;
		
	public Estudante(Integer estudante) {
		
		this.estudante = estudante;
	}

	public Integer getEstudante() {
		return estudante;
	}
	
	public void setEstudante(Integer estudante) {
		this.estudante = estudante;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estudante == null) ? 0 : estudante.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estudante other = (Estudante) obj;
		if (estudante == null) {
			if (other.estudante != null)
				return false;
		} else if (!estudante.equals(other.estudante))
			return false;
		return true;
	}
	
	

}
