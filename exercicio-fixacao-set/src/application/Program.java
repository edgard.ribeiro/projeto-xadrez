package application;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import entities.Estudante;

public class Program {

	public static void main(String[] args) throws ParseException{
		
		Scanner sc = new Scanner(System.in);
		
		Set<Estudante> a = new HashSet<>();
		Set<Estudante> b = new HashSet<>();
		Set<Estudante> c = new HashSet<>();
		
		System.out.print("Quantos alunos no curso A: ");
		int n = sc.nextInt();
		for(int i = 0; i < n; i++) {
			int numero = sc.nextInt();
			a.add(new Estudante(numero));
		}
		
		System.out.print("Quantos alunos no curso B: ");
		n = sc.nextInt();
		for(int i = 0; i < n; i++) {
			int numero = sc.nextInt();
			b.add(new Estudante(numero));
		}
		
		System.out.print("Quantos alunos no curso C: ");
		n = sc.nextInt();
		for(int i = 0; i < n; i++) {
			int numero = sc.nextInt();
			c.add(new Estudante(numero));
		}
						
		Set<Estudante> total = new HashSet<>();
		total.addAll(a);
		total.addAll(b);
		total.addAll(c);
				
		System.out.print("Total de alunos: " + total.size());
		sc.close();

	}

}
